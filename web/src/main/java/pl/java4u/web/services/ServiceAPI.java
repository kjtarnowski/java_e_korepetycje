package pl.java4u.web.services;

import pl.java4u.web.EntityNotFoundException;

import java.util.List;

public interface ServiceAPI<T> {

    T findByID(Long id) throws EntityNotFoundException;

    List<T> findAll();

    T create(T type);

    T update(T type);

    T delete(T type);

}
