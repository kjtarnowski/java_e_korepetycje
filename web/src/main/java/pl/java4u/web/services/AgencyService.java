package pl.java4u.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dao.model.Agency;
import pl.java4u.web.dao.repository.AgencyRepository;
import pl.java4u.web.dto.AgencyDto;


import java.util.List;
import java.util.stream.Collectors;
import static pl.java4u.web.mapper.AgencyMapper.INSTANCE;

@Service
public class AgencyService implements ServiceAPI<AgencyDto> {

    @Autowired
    AgencyRepository agencyRepository;

    @Override
    public AgencyDto findByID(Long id) throws EntityNotFoundException {
        Agency agency = agencyRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return INSTANCE.agencyToAgencyDto(agency);

    }

    @Override
    public List<AgencyDto> findAll() {
        return INSTANCE.toAgenciesDTOs((List<Agency>) agencyRepository.findAll());
    }

    @Override
    public AgencyDto create(AgencyDto type) {
        Agency agency = INSTANCE.agencyDtoToAgency(type);
        Agency saved = agencyRepository.save(agency);
        return INSTANCE.agencyToAgencyDto(saved);

    }

    @Override
    public AgencyDto update(AgencyDto type) {
        Agency agency = INSTANCE.agencyDtoToAgency(type);
        Agency saved = agencyRepository.save(agency);
        return INSTANCE.agencyToAgencyDto(saved);
    }

    @Override
    public AgencyDto delete(AgencyDto type) {
        Agency agency = INSTANCE.agencyDtoToAgency(type);
        agencyRepository.delete(agency);
        return type;
    }
}
