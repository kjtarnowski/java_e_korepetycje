package pl.java4u.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dao.model.Customer;
import pl.java4u.web.dao.repository.CustomerRepository;
import pl.java4u.web.dto.CustomerDto;

import java.util.List;

import static pl.java4u.web.mapper.CustomerMapper.INSTANCE;

@Service
public class CustomerService implements ServiceAPI<CustomerDto> {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public CustomerDto findByID(Long id) throws EntityNotFoundException {
        Customer customer = customerRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return INSTANCE.customerToCustomerDto(customer);

    }

    @Override
    public List<CustomerDto> findAll() {
        return INSTANCE.toCustomersDTOs((List<Customer>) customerRepository.findAll());
    }

    @Override
    public CustomerDto create(CustomerDto type) {
        Customer customer = INSTANCE.customerDtoToCustomer(type);
        customerRepository.save(customer);
        return type;

    }

    @Override
    public CustomerDto update(CustomerDto type) {
        Customer customer = INSTANCE.customerDtoToCustomer(type);
        customerRepository.save(customer);
        return type;
    }

    @Override
    public CustomerDto delete(CustomerDto type) {
        Customer customer = INSTANCE.customerDtoToCustomer(type);
        customerRepository.delete(customer);
        return type;
    }
}
