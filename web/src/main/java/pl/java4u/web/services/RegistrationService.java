package pl.java4u.web.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dao.model.Registration;
import pl.java4u.web.dao.repository.RegistrationRepository;
import pl.java4u.web.dto.RegistrationDto;

import java.util.List;
import java.util.stream.Collectors;
import static pl.java4u.web.mapper.RegistrationMapper.INSTANCE;

@Service
public class RegistrationService implements ServiceAPI<RegistrationDto> {

    @Autowired
    RegistrationRepository registrationRepository;

    @Override
    public RegistrationDto findByID(Long id) throws EntityNotFoundException {
        Registration registration = registrationRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return INSTANCE.registrationToRegistrationDto(registration);

    }

    @Override
    public List<RegistrationDto> findAll() {
        return INSTANCE.toRegistrationsDTOs((List<Registration>) registrationRepository.findAll());
    }

    @Override
    public RegistrationDto create(RegistrationDto type) {
        Registration registration = INSTANCE.registrationDtoToRegistration(type);
        registrationRepository.save(registration);
        return type;

    }

    @Override
    public RegistrationDto update(RegistrationDto type) {
        Registration registration = INSTANCE.registrationDtoToRegistration(type);
        registrationRepository.save(registration);
        return type;
    }

    @Override
    public RegistrationDto delete(RegistrationDto type) {
        Registration registration = INSTANCE.registrationDtoToRegistration(type);
        registrationRepository.delete(registration);
        return type;
    }
}
