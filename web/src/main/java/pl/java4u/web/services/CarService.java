package pl.java4u.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dao.model.Car;
import pl.java4u.web.dao.repository.CarRepository;
import pl.java4u.web.dto.CarDto;
import pl.java4u.web.dto.RegistrationDto;

import java.util.List;
import java.util.stream.Collectors;
import static pl.java4u.web.mapper.CarMapper.INSTANCE;

@Service
public class CarService implements ServiceAPI<CarDto> {

    @Autowired
    CarRepository carRepository;

    @Override
    public CarDto findByID(Long id) throws EntityNotFoundException {
        Car car = carRepository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);
        return INSTANCE.carToCarDto(car);

    }

    @Override
    public List<CarDto> findAll() {
        return INSTANCE.toCarDTOs((List<Car>) carRepository.findAll());
    }

    @Override
    public CarDto create(CarDto type) {
        Car car = INSTANCE.carDtoToCar(type);
        carRepository.save(car);
        return type;

    }

    @Override
    public CarDto update(CarDto type) {
        Car car = INSTANCE.carDtoToCar(type);
        carRepository.save(car);
        return type;
    }

    @Override
    public CarDto delete(CarDto type) {
        Car car = INSTANCE.carDtoToCar(type);
        carRepository.delete(car);
        return type;
    }
}
