package pl.java4u.web.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dao.model.Part;
import pl.java4u.web.dao.repository.PartRepository;
import pl.java4u.web.dto.PartDto;

import java.util.List;
import java.util.stream.Collectors;
import static pl.java4u.web.mapper.PartMapper.INSTANCE;

@Service
public class PartService implements ServiceAPI<PartDto> {

    @Autowired
    PartRepository partRepository;

    @Override
    public PartDto findByID(Long id) throws EntityNotFoundException {
        Part part = partRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return INSTANCE.partToPartDto(part);

    }

    @Override
    public List<PartDto> findAll() {
        return INSTANCE.toPartsDTOs((List<Part>) partRepository.findAll());
    }

    @Override
    public PartDto create(PartDto type) {
        Part part = INSTANCE.partDtoToPart(type);
        partRepository.save(part);
        return type;

    }

    @Override
    public PartDto update(PartDto type) {
        Part part = INSTANCE.partDtoToPart(type);
        partRepository.save(part);
        return type;
    }

    @Override
    public PartDto delete(PartDto type) {
        Part part = INSTANCE.partDtoToPart(type);
        partRepository.delete(part);
        return type;
    }
}
