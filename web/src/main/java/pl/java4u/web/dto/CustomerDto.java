package pl.java4u.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.java4u.web.dao.model.Car;
import pl.java4u.web.dao.model.Registration;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerDto {


    private Long idDto;
    private String firstNameDto;
    private String lastNameDto;
    private String emailDto;
    private Long phoneNumberDto;
    private List<CarDto> carsDto;
    private List<RegistrationDto> registrationsDto;

}
