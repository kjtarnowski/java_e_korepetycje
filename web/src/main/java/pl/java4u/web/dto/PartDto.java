package pl.java4u.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.java4u.web.dao.model.Car;
import pl.java4u.web.dao.model.Registration;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartDto {

    private Long idDto;
    private String partNameDto;
    private CarDto carDto;
    private RegistrationDto registrationDto;

}
