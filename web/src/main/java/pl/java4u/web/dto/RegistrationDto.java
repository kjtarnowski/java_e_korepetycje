package pl.java4u.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.java4u.web.dao.model.Agency;
import pl.java4u.web.dao.model.Car;
import pl.java4u.web.dao.model.Customer;
import pl.java4u.web.dao.model.Part;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegistrationDto {

    private Long idDto;
    private String descriptionDto;
    private LocalDate registrationDateDto;
    private AgencyDto agencyDto;
    private CustomerDto customerDto;
    private CarDto carDto;
    private List<PartDto> partsDto;
}
