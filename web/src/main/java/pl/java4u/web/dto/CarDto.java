package pl.java4u.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.java4u.web.dao.model.Customer;
import pl.java4u.web.dao.model.Part;
import pl.java4u.web.dao.model.Registration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Valid
public class CarDto {

    private Long idDto;
    @NotNull
    private CustomerDto customerDto;
    private List<PartDto> partsDto; // List<PartDto>
    private List<RegistrationDto> registrationsDto;

}
