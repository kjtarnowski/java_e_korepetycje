package pl.java4u.web.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.java4u.web.dao.model.Agency;
import pl.java4u.web.dto.AgencyDto;

import java.util.List;

@Mapper
public interface AgencyMapper {

    AgencyMapper INSTANCE = Mappers.getMapper(AgencyMapper.class);

    @Mapping(source = "id", target = "idDto")
    @Mapping(source = "adress", target = "adressDto")
    AgencyDto agencyToAgencyDto(Agency agency);

    @InheritInverseConfiguration
    Agency agencyDtoToAgency(AgencyDto agencyDto);

    List<AgencyDto> toAgenciesDTOs(List<Agency> agencies);

    List<Agency> toAgencies(List<AgencyDto> agenciesDto);

}
