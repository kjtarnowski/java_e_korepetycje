package pl.java4u.web.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.java4u.web.dao.model.Registration;
import pl.java4u.web.dto.RegistrationDto;

import java.util.List;

@Mapper
public interface RegistrationMapper {

    RegistrationMapper INSTANCE = Mappers.getMapper(RegistrationMapper.class);

    @Mapping(source = "id", target = "idDto")
    @Mapping(source = "description", target = "descriptionDto")
    @Mapping(source = "registrationDate", target = "registrationDateDto")
    @Mapping(source = "agency", target = "agencyDto")
    @Mapping(source = "customer", target = "customerDto")
    @Mapping(source = "car", target = "carDto")
    @Mapping(source = "parts", target = "partsDto")
    RegistrationDto registrationToRegistrationDto(Registration registration);

    @InheritInverseConfiguration
    Registration registrationDtoToRegistration(RegistrationDto registrationDto);

    List<RegistrationDto> toRegistrationsDTOs(List<Registration> registrations);

    List<Registration> toRegistrations(List<RegistrationDto> registrationsDto);
}
