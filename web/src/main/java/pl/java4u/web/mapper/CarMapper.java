package pl.java4u.web.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import pl.java4u.web.dao.model.Car;
import pl.java4u.web.dto.CarDto;

import java.util.List;

@Mapper
public interface CarMapper {

    CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);


    @Mapping(source = "id", target = "idDto")
    @Mapping(source = "customer", target = "customerDto")
    @Mapping(source = "parts", target = "partsDto")
    @Mapping(source = "registrations", target = "registrationsDto")
    CarDto carToCarDto(Car car);

    @InheritInverseConfiguration
    Car carDtoToCar(CarDto carDto);

    List<CarDto> toCarDTOs(List<Car> cars);

    List<Car> toCars(List<CarDto> carsDto);
}

