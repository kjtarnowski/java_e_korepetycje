package pl.java4u.web.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.java4u.web.dao.model.Part;
import pl.java4u.web.dto.PartDto;

import java.util.List;

@Mapper
public interface PartMapper {

    PartMapper INSTANCE = Mappers.getMapper(PartMapper.class);


    @Mapping(source = "id", target = "idDto")
    @Mapping(source = "partName", target = "partNameDto")
    @Mapping(source = "car", target = "carDto")
    @Mapping(source = "registration", target = "registrationDto")
    PartDto partToPartDto(Part part);

    @InheritInverseConfiguration
    Part partDtoToPart(PartDto partDto);

    List<PartDto> toPartsDTOs(List<Part> parts);

    List<Part> toParts(List<PartDto> partsDto);
}
