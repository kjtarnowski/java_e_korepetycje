package pl.java4u.web.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.java4u.web.dao.model.Customer;
import pl.java4u.web.dto.CustomerDto;

import java.util.List;

@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Mapping(source = "id", target = "idDto")
    @Mapping(source = "firstName", target = "firstNameDto")
    @Mapping(source = "lastName", target = "lastNameDto")
    @Mapping(source = "email", target = "emailDto")
    @Mapping(source = "phoneNumber", target = "phoneNumberDto")
    @Mapping(source = "cars", target = "carsDto")
    @Mapping(source = "registrations", target = "registrationsDto")
    CustomerDto customerToCustomerDto(Customer customer);

    @InheritInverseConfiguration
    Customer customerDtoToCustomer(CustomerDto customerDto);

    List<CustomerDto> toCustomersDTOs(List<Customer> customers);

    List<Customer> toCustomers(List<CustomerDto> customersDto);
}
