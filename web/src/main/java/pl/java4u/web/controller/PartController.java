package pl.java4u.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.java4u.web.A;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dto.PartDto;
import pl.java4u.web.services.PartService;

import java.net.URI;
import java.util.List;

@RestController
public class PartController{

    @Autowired
    PartService partService;

    //@RequestMapping(method = RequestMethod.GET, path = "/part/all")
    @GetMapping("/part/all")
    public ResponseEntity<List<PartDto>> findAll(){
        return ResponseEntity.ok(partService.findAll());
    }

    @GetMapping("/part/{id}")
    public ResponseEntity<PartDto> findById(@PathVariable Long id) throws EntityNotFoundException {
        return ResponseEntity.ok(partService.findByID(id));
    }

    @PostMapping("/part/")
    public ResponseEntity<PartDto> createPart(@RequestBody PartDto partDto)  {
        PartDto part = partService.create(partDto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(part.getIdDto()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping ("/part/{id}")
    public ResponseEntity<PartDto> deletePart(@PathVariable Long id) throws EntityNotFoundException {
        partService.delete(partService.findByID(id));
        return ResponseEntity.noContent().build();
    }

}