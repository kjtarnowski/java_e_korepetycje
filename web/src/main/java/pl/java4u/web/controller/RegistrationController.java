package pl.java4u.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.java4u.web.A;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dto.RegistrationDto;
import pl.java4u.web.services.RegistrationService;

import java.net.URI;
import java.util.List;

@RestController
public class RegistrationController{

    @Autowired
    RegistrationService registrationService;

    //@RequestMapping(method = RequestMethod.GET, path = "/registration/all")
    @GetMapping("/registration/all")
    public ResponseEntity<List<RegistrationDto>> findAll(){
        return ResponseEntity.ok(registrationService.findAll());
    }

    @GetMapping("/registration/{id}")
    public ResponseEntity<RegistrationDto> findById(@PathVariable Long id) throws EntityNotFoundException {
        return ResponseEntity.ok(registrationService.findByID(id));
    }

    @PostMapping("/registration/")
    public ResponseEntity<RegistrationDto> createRegistration(@RequestBody RegistrationDto registrationDto)  {
        RegistrationDto registration = registrationService.create(registrationDto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(registration.getIdDto()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping ("/registration/{id}")
    public ResponseEntity<RegistrationDto> deleteRegistration(@PathVariable Long id) throws EntityNotFoundException {
        registrationService.delete(registrationService.findByID(id));
        return ResponseEntity.noContent().build();
    }

}