package pl.java4u.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.java4u.web.A;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dto.AgencyDto;
import pl.java4u.web.services.AgencyService;

import java.net.URI;
import java.util.List;

@RestController
public class AgencyController{

    @Autowired
    AgencyService agencyService;

    //@RequestMapping(method = RequestMethod.GET, path = "/agency/all")
    @GetMapping("/agency/all")
    public ResponseEntity<List<AgencyDto>> findAll(){
        return ResponseEntity.ok(agencyService.findAll());
    }

    @GetMapping("/agency/{id}")
    public ResponseEntity<AgencyDto> findById(@PathVariable Long id) throws EntityNotFoundException {
        return ResponseEntity.ok(agencyService.findByID(id));
    }

    @PostMapping("/agency/")
    public ResponseEntity<AgencyDto> createAgency(@RequestBody AgencyDto agencyDto)  {
        AgencyDto agency = agencyService.create(agencyDto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(agency.getIdDto()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping ("/agency/{id}")
    public ResponseEntity<AgencyDto> deleteAgency(@PathVariable Long id) throws EntityNotFoundException {
        agencyService.delete(agencyService.findByID(id));
        return ResponseEntity.noContent().build();
    }

}
