package pl.java4u.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.java4u.web.A;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dto.CustomerDto;
import pl.java4u.web.services.CustomerService;

import java.net.URI;
import java.util.List;

@RestController
public class CustomerController{

    @Autowired
    CustomerService customerService;

    //@RequestMapping(method = RequestMethod.GET, path = "/customer/all")
    @GetMapping("/customer/all")
    public ResponseEntity<List<CustomerDto>> findAll(){
        return ResponseEntity.ok(customerService.findAll());
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<CustomerDto> findById(@PathVariable Long id) throws EntityNotFoundException {
        return ResponseEntity.ok(customerService.findByID(id));
    }

    @PostMapping("/customer/")
    public ResponseEntity<CustomerDto> createCustomer(@RequestBody CustomerDto customerDto)  {
        CustomerDto customer = customerService.create(customerDto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(customer.getIdDto()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping ("/customer/{id}")
    public ResponseEntity<CustomerDto> deleteCustomer(@PathVariable Long id) throws EntityNotFoundException {
        customerService.delete(customerService.findByID(id));
        return ResponseEntity.noContent().build();
    }

}