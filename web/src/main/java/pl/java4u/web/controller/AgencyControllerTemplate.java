package pl.java4u.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pl.java4u.web.dto.AgencyDto;
import pl.java4u.web.services.AgencyService;

@Controller
public class AgencyControllerTemplate {

    @Autowired
    AgencyService agencyService;

    @GetMapping("/agency/create")
    String saveAgnecy(Model model){
        model.addAttribute("agency", new AgencyDto());
        return "createAgency";
    }

    @PostMapping(path = "/agency/create") //", consumes = "application/json;charset=UTF-8"
    String addAgency(@ModelAttribute AgencyDto agencyDto){
        agencyService.create(agencyDto);
        return "redirect:/result";
    }

}
