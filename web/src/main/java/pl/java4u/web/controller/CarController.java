package pl.java4u.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.java4u.web.A;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dto.CarDto;
import pl.java4u.web.services.CarService;

import java.net.URI;
import java.util.List;

@RestController
public class CarController{

    @Autowired
    CarService carService;

    //@RequestMapping(method = RequestMethod.GET, path = "/car/all")
    @GetMapping("/car/all")
    public ResponseEntity<List<CarDto>> findAll(){
        return ResponseEntity.ok(carService.findAll());
    }

    @GetMapping("/car/{id}")
    public ResponseEntity<CarDto> findById(@PathVariable Long id) throws EntityNotFoundException {
        return ResponseEntity.ok(carService.findByID(id));
    }

    @PostMapping("/car/")
    public ResponseEntity<CarDto> createCar(@RequestBody CarDto carDto)  {
        CarDto car = carService.create(carDto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(car.getIdDto()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping ("/car/{id}")
    public ResponseEntity<CarDto> deleteCar(@PathVariable Long id) throws EntityNotFoundException {
        carService.delete(carService.findByID(id));
        return ResponseEntity.noContent().build();
    }

}