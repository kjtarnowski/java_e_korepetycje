package pl.java4u.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TodoController {

    private String message = "task1";

    @GetMapping("/ToDo")
    public String todoGet(Model model){
        model.addAttribute("task",this.message);
        return "ToDo";
    }

    @PostMapping("/ToDo")
    public String todoPost(ModelMap model){ //@RequestParam(value= "text") String text
        //model.put("taskText",text);
        return "ToDo";
    }

}
