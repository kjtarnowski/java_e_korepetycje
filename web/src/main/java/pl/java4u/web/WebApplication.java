package pl.java4u.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

@SpringBootApplication
public class WebApplication implements CommandLineRunner {

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
	@Qualifier("a")
    I z;

	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
        z.logme();
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        //Arrays.stream(beanDefinitionNames).forEach(System.out::println);
    }
}
