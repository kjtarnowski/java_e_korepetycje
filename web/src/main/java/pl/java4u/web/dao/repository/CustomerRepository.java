package pl.java4u.web.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.java4u.web.dao.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository <Customer, Long> {
}
