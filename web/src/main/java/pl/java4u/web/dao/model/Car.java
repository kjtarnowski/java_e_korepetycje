package pl.java4u.web.dao.model;

//zamodelowac baze warsztatu

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name= "Customer_ID")
    private Customer customer;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL,orphanRemoval = true)
//    @JoinColumn(name= "Part_ID")
    private List<Part> parts;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL,orphanRemoval = true)
//    @JoinColumn(name= "Order_ID")
    private List<Registration> registrations;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    public List<Registration> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
