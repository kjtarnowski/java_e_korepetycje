package pl.java4u.web.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.java4u.web.dao.model.Registration;

@Repository
public interface RegistrationRepository extends CrudRepository<Registration, Long> {
}