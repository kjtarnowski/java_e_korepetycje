package pl.java4u.web.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.java4u.web.dao.model.Part;

@Repository
public interface PartRepository extends CrudRepository<Part, Long> {
}