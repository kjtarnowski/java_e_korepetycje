package pl.java4u.web.dao.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Agency {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String adress;

    @OneToMany(mappedBy = "agency",cascade = CascadeType.ALL,orphanRemoval = true)
//    @JoinColumn(name= "Order_ID")
    private List<Registration> registrations;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Agency agency = (Agency) o;
        return Objects.equals(id, agency.id) &&
                Objects.equals(adress, agency.adress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, adress);
    }

    @Override
    public String toString() {
        return "Agency{" +
                "id=" + id +
                ", adress='" + adress + '\'' +
                '}';
    }
}
