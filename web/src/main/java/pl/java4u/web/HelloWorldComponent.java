package pl.java4u.web;

import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
public class HelloWorldComponent {

    private List<Message> messages = new ArrayList();

    @GetMapping(name = "/hello")
    public String helloWorld(){
        return "Hello World";
    }

    @GetMapping(name = "/message", produces = APPLICATION_JSON_UTF8_VALUE)
    public List<Message> getMessage(){
        return  messages;
    }

    @PostMapping(name = "/message", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public List<Message> addToMeessageToList(@RequestBody Message message){
        messages.add(message);
        return messages;
    }

    @DeleteMapping("/message/{id}")
    public void removeElement(@PathVariable("id") int index){
        messages.remove(index);
    }

    @PostConstruct
    public void fillList(){
        messages.add(new Message("Hello"));
        messages.add(new Message("World"));
        messages.add(new Message("Something"));
        System.out.println("Uzupelniam liste");
    }

    //Dolozyc walidacje media type dla jsona
    //Zainstalowac narzedzie do obslugi restow, moze byc np. Postman
    //Zaimplementowac metody POST,PUT,DELETE ktore beda wykonywac operacje crudowe na liscie messagy
}
