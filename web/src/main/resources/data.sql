INSERT INTO CUSTOMER (ID, first_Name, Last_Name,email,phone_Number) VALUES ('1001','Jan','Kowalski', 'Kowalski@java4u.pl','111');
INSERT INTO CUSTOMER (ID,first_Name, Last_Name,email,phone_Number) VALUES ('1002','Paweł','Nowak', 'Nowak@java4u.pl','222');
INSERT INTO CUSTOMER (ID,first_Name, Last_Name,email,phone_Number) VALUES ('1003','JAcek','Iksinski', 'Iksinski@java4u.pl','333');

INSERT INTO AGENCY (ID,ADRESS) VALUES ('1001','Lodz, Sienkiewicza 112');

INSERT INTO CAR (ID,Customer_ID) VALUES ('1001','1001');
INSERT INTO CAR (ID,Customer_ID) VALUES ('1002','1002');
INSERT INTO CAR (ID,Customer_ID) VALUES ('1003','1003');


INSERT INTO REGISTRATION (ID,Registration_Date,Description,Agency_ID,Car_ID,Customer_ID ) VALUES ('1001',to_date('01-12-18', 'DD-MM-RR'),'BrakePad replacement','1001','1001','1001');
INSERT INTO REGISTRATION (ID,Registration_Date,Description,Agency_ID,Car_ID,Customer_ID ) VALUES ('1002',to_date('02-12-18', 'DD-MM-RR'),'Radiator replacement','1001','1002','1002');
INSERT INTO REGISTRATION (ID,Registration_Date,Description,Agency_ID,Car_ID,Customer_ID ) VALUES ('1003',to_date('03-12-18', 'DD-MM-RR'),'BrakePad replacement','1001','1003','1003');

INSERT INTO PART (ID,Part_Name,Car_ID,Registration_ID ) VALUES ('1001','BrakePad','1001','1001');
INSERT INTO PART (ID,Part_Name,Car_ID,Registration_ID ) VALUES ('1002','Radiator','1002','1002');
INSERT INTO PART (ID,Part_Name,Car_ID,Registration_ID ) VALUES ('1003','OilFilter','1003','1003');