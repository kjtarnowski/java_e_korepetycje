package pl.java4u.web.dao.repository;

import org.apache.commons.collections4.IteratorUtils;
import org.assertj.core.util.Streams;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.java4u.web.dao.model.Agency;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AgencyRepositoryTest {

    @Autowired
    AgencyRepository agencyRepository;

    @Test
     public void should_save_agency_to_db() throws Exception {
        //given
        Agency agency = Agency.builder().adress("Łódź Tuwima 10").build();
        //when
        Agency savedAgency = agencyRepository.save(agency);
        //then
        Agency agencyFromRepository = agencyRepository.findById(savedAgency.getId()).orElseThrow(Exception::new);
        assertEquals(savedAgency,agencyFromRepository);
        //when
        //delete
        //then
        //is delete?
    }

    @Test(expected = Exception.class)
    public void should_delete_agency_from_db() throws Exception {
        //given
        Agency agency = Agency.builder().adress("Łódź Tuwima 10").build();
        //when
        Agency savedAgency = agencyRepository.save(agency);
        //then
        Agency agencyFromRepository = agencyRepository.findById(savedAgency.getId()).orElseThrow(Exception::new);
        //assert
        //when
        agencyRepository.delete(agencyFromRepository);
        //then
        List<Agency> collectAgencies = IteratorUtils.toList(agencyRepository.findAll().iterator());
//        Assert.assertThat(addresses, Matchers.("Łódź Tuwima 10"));
        assertFalse(collectAgencies.stream()
                .anyMatch(item -> "Łódź Tuwima 10"
                        .equals(item.getAdress())));
//        try{
//            //findby
//            fail();
//        } catch (Exception e){
//            e.getMessage()
//        }
       agencyRepository.findById(savedAgency.getId()).orElseThrow(Exception::new);
       fail();
//        //when
        //delete
        //then
        //is delete?
    }

}