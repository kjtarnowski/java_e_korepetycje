package pl.java4u.web.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.java4u.web.EntityNotFoundException;
import pl.java4u.web.dto.AgencyDto;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AgencyServiceTest {

    @Autowired
    AgencyService agencyService;

    @Test
    public void create() {
        //given
        AgencyDto agencyDto = AgencyDto.builder().adressDto("Lodz Tumima 10").build();
        //when
        AgencyDto createdAgencyDto = agencyService.create(agencyDto);
        //then
        try {
            AgencyDto byID = agencyService.findByID(createdAgencyDto.getIdDto());
            assertNotNull(byID);
        } catch (EntityNotFoundException e) {
            fail(e.getMessage());
        }
        //when
        agencyService.delete(createdAgencyDto);
        //then
        try {
            AgencyDto byID = agencyService.findByID(createdAgencyDto.getIdDto());
            fail();
        } catch (EntityNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}